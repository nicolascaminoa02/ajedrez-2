package Test;

import Piezas.peon;
import org.junit.Test;
import static org.junit.Assert.*;

public class PeonTest {
    peon PeonPrueba = new peon();

    @Test
    public void pruebaGritar() {
        assertEquals("Soy el Peon!", PeonPrueba.gritar());
    }

    @Test
    public void pruebaColor() {
        assertEquals("Soy el Peon!", PeonPrueba.color());
    }

    @Test
    public void pruebaPosicion() {
        assertEquals("Estoy en la fila 7", PeonPrueba.posicion());
    }
}
